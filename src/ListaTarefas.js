import React, { useEffect, useState } from 'react';
import instanciaAxios from './ajax/instanciaAxios';
import './listaTarefas.css'


const ListaTarefas = () => {

    const [listaCategorias, setListaCategorias] = useState ([]);
    const [listaTarefas, setListaTarefas] = useState ([]);
    const [listaDias, setListaDias] = useState ([]);
    const [descricaoNovoItem, setDescricaoNovoItem] = useState ('');
    const [categoriaNovoItem, setCategoriaNovoItem] = useState ('');
    const [diaNovoItem, setDiaNovoItem] = useState('');
    const [alertaNovoItem, setAlertaNovoItem] = useState('desligado');
    
    
    const pegarCategorias = async () => {

        try{

        const resposta = await instanciaAxios.get('../json/categorias.json');
        console.log(`Resultado: ${ JSON.stringify( resposta.data ) } `);

        setListaCategorias(resposta.data.categorias);

        } catch (error) {
            console.log(error.message);
        }
    };

    const pegarTarefas = async () => {

        try{

        const resposta = await instanciaAxios.get('../json/tarefas.json');
        console.log(`Resultado: ${ JSON.stringify( resposta.data ) } `);

        setListaTarefas(resposta.data.tarefas);

        } catch (error) {
            console.log(error.message);
        }
    };

    const pegarDias = async () => {

        try {
          const resposta = await instanciaAxios.get('../json/dias.json');
          setListaDias(resposta.data.dias);
        } catch (error) {
          console.log(error.message);
        }
    
      };
    

    useEffect(() => {
        pegarCategorias();
        pegarTarefas();
        pegarDias();
    }, []);


    const OpcoesDiasComponente = () => {
        if( listaDias.length > 0 ) {
          const listaDiasJSX = listaDias.map( ( item ) => {
              console.log(item)
            return (
                <div>
                    <input 
                    type='radio' 
                    name='dia' 
                    value={ item.id } 
                    id={ `dia-${item.valor}` } 
                    onChange={ (evento) => setDiaNovoItem( evento.target.value ) } 
                    checked={ item.id === diaNovoItem } />
                    <label htmlFor={ `dia-${item.valor}` }>
                    { item.valor }
                    </label>
              </div>
            );
          } );
        
          return listaDiasJSX;
        } else { 
          return null;
        }
      };
    

    const OpcoesCategoriasComponente = () => {

        if( listaCategorias.length > 0 ) {
            const listaCategoriasJSX = listaCategorias.map( ( item ) => {
                return (
                    <option 
                        key={ item.id } 
                        value={ item.id } >
                        {item.descricao}
                    </option>
                );

            } );

            return listaCategoriasJSX;
        } else {
            return null;
        }
    };

    const AlertaIconeComponente = () => {
        return ( 
          <img  
          src='/images/medal.png' style={ { width: '15px', marginLeft: '15px' } } />
        );
      };
    

    const CorpoTabelaComponente = () => {

        if( listaTarefas.length > 0 ) {
            console.log(listaTarefas)
        return(
                <tbody>
                    { listaTarefas.map( (item) => {
                        return (
                            < LinhaTabelaComponente 
                            key={ item.id }
                            id={ item.id }
                            descricao={ item.descricao } 
                            idCategoria= {item.idCategoria }
                            idDia= { item.idDia }
                            alerta= { item.alerta } />
                        );
                    } ) }
                </tbody>
            );
        } else {
            return null;
        }
    };

    const LinhaTabelaComponente = ( { id, descricao, idCategoria, idDia, alerta, } ) => {
        
        const _categoria = listaCategorias ? listaCategorias.find( item => item.id === idCategoria ) : null;
        console.log(_categoria)
        const _dia = listaDias ? listaDias.find( item => item.id === idDia ) : null;
        const _alerta = alerta === 'ligado' ? <AlertaIconeComponente /> : null;
        return (
            <tr>
                <td>
                    { descricao }
                    { alerta === 'ligado' ? <AlertaIconeComponente /> : null }
                    </td>
                <td>{ _categoria ? _categoria.descricao : null }</td>
                <td>{ _dia ? _dia.valor : null }</td>
                 <td>
                     <img 
                     className= 'icone'
                     src='/images/delete.png' style={ { width: '15px', marginLeft: '15px' } } 
                     onClick={ () => { removerItem(id) } }
                     />
                 </td>
            </tr>

        );
    };

    const incluirItem = () => {

        if( categoriaNovoItem && descricaoNovoItem ) {

            
            const indiceUltimoElemento = listaTarefas.length - 1;
            const ultimoElemento = listaTarefas[ indiceUltimoElemento ];
            const idUltimoElemento =  ultimoElemento.id;
            const idNovoItem = parseInt ( idUltimoElemento ) + 1;

            console.log(ultimoElemento)
            

            const novoItem = {
                "id": "idNovoItem",
                "descricao": descricaoNovoItem,
                "idCategoria": categoriaNovoItem,
                "idDia": diaNovoItem,
                "alerta": alertaNovoItem
            };

            setListaTarefas( [ ...listaTarefas, novoItem ] );

        } else { 
            alert('Por favor, preencha todos os campos');
        }
    };

    const removerItem = ( idSelecionado ) => {
        console.log( `O id selecionado foi: ${ idSelecionado }` );

        const _listaTarefas = listaTarefas.filter( (item) => {
            return item.id !== idSelecionado;
          } );
      
          setListaTarefas( _listaTarefas );
      
    };

    return (
        <>
            <h1>Avaliação de Hábitos Alimentares</h1>

            <div id='container' >
                <div id='box-novo-item' >
                    <h3>Avaliação do dia</h3>
                    <div className='novo-item-campo-input-select'>
                        <label>Descrição das refeições do dia:</label>
                        <input type='text' 
                        onChange= { (evento) => setDescricaoNovoItem( evento.target.value ) } />
                    </div>

                    <div className='novo-item-campo-input-select'>
                        <label> Avaliação </label>
                        <select 
                        value= { categoriaNovoItem}
                        onChange= { (evento) => setCategoriaNovoItem( evento.target.value ) }>
                            <option value={-1}>Selecione uma opção</option>
                            <OpcoesCategoriasComponente />
                        </select>
                    </div>

                    <div>
                        
                        <label>Dia:</label>
                        <OpcoesDiasComponente />
                        
                    </div>
                        <br></br>
                    <div>
                        <input 
                        type='checkbox' 
                        id='campo-alerta' 
                        name='campo-alerta' 
                        onChange={ () => { setAlertaNovoItem( 
                            alertaNovoItem === 'ligado' ? 'desligado' : 'ligado' ) } } />
                        <label htmlFor='campo-alerta'>Fez exercício físico?</label>

                    </div>
                        <br></br>
                    <button
                    id='botao-registrar'
                    onClick={ () => incluirItem () } 
                    >Registrar</button>
                </div>
                
                <div id='lista-tarefas' >
                    <table>

                        <thead>
                            <th>Descrição das refeições do dia</th>
                            <th>Avaliação</th>
                            <th>Dia</th>
                            <th>Ações</th>
                        </thead>

                        <CorpoTabelaComponente />

                        

                        <tfoot>
                            <tr>
                                <td colSpan="5">Total de avaliações: { listaTarefas.length } </td>
                            </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </>
    );

}

export default ListaTarefas;

